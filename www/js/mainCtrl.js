(function () {

  angular.module('standiBovin')

    .controller('inputPanelCtrl', function ($scope, $ionicScrollDelegate, $location, $ionicHistory, animalsSvc, authService, angularAuth0, Analytics) {

      $scope.location = window.location;

      // get default vars from service
      var vars = animalsSvc.vars;

      $scope.data = vars.data;

      // language texts
      $scope.text = vars.text;

      // bovin type
      $scope.types = vars.values.types;

      // bovin breeds
      $scope.breeds = vars.values.breeds;

      // bovin input weight
      $scope.inputWeights = vars.values.inputWeights;

      // bovin feeds
      $scope.feeds = vars.values.feeds;

      // list of additives
      $scope.additives = vars.values.additives;


      // init angular variables

      // selected values
      $scope.input = vars.input;

      // compaison values for result page
      $scope.comp = vars.comp;

      $scope.control = vars.control;

      $scope.params = {
        "feed1": {},
        "feed2": {}
      };


      $scope.caseFrance = function () {
        caseFrance = vars.country === "France";
        return caseFrance;
      };
      // console.log('caseFrance : ' + caseFrance);

      $ionicHistory.clearHistory();

      // init language
      $scope.languages = vars.languages;
      $scope.input.language = vars.settings.language;

      /*
          get language texts from selected language
          */
      $scope.setLanguage = function () {
        animalsSvc.text($scope.input.language).then(function (res) {
          animalsSvc.copyObject(vars.text, res.data);
        }, function (err) {
        });
      };

      $scope.setDataFromLanguage = function () {
        console.log('setDataFromLanguage');

        var profile = JSON.parse(localStorage.getItem('profile'));
        console.log('profil : ' + profile);
        console.log('profile.app_metadata : ' + profile['app_metadata']);
        console.log('profile.app_metadata.key : ' + profile['app_metadata']['key']);
        var key = profile.app_metadata.key;
        console.log('profil : ' + key);
        // angularAuth0.getProfile(authResult.idToken, function (error, profileData) {
        //   console.log('getProfile callback');
        //   if (error) {
        //     return console.log(error);
        //   }

        // get decryption key in user metadata
        // var key = profileData.app_metadata.key ? profileData.app_metadata.key : null;

        console.log('$scope.input.language : ' + $scope.input.language);
        if ($scope.input.language === 'Bresil') {
          console.log('find Bresil');
          vars.country = "Bresil";
        } else if ($scope.input.language === 'Français') {
          console.log('find Français');
          vars.country = 'France';
        } else {
          console.log('find no language ? ' + $scope.input.language);
        }

        animalsSvc.clear();
        // vars.country = profileData.app_metadata.country ? profileData.app_metadata.country : null;

        //updates data from bovin_aes.json
        animalsSvc.getData(key);
        // });
      };

      $scope.setLanguage();

      $scope.translate = function (text) {
        if (typeof text == "string") {
          if ($scope.text[text]) {
            text = $scope.text[text];
          }
        } else if (typeof text == "object") {

        }
      };

      /*
          get list of breeds from selected type
           */
      $scope.getValuesFromType = function () {

        if ($scope.data) {
          if ($scope.input.type) {
            $scope.breeds.length = 0;
            animalsSvc.copyArray($scope.breeds, Object.keys($scope.data.country[vars.country].type[$scope.input.type].breed));

            // reset all other values
            $scope.inputWeights.length = 0;
            $scope.input.inputWeight = null;
            $scope.feeds.length = 0;
            $scope.input.feed1 = null;
            $scope.input.feed2 = null;
          }
        }
      };

      /*
          get list of input weights from selected breed
           */
      $scope.getValuesFromBreed = function () {

        if ($scope.data) {
          if ($scope.input.breed) {
            $scope.inputWeights.length = 0;
            animalsSvc.copyArray($scope.inputWeights, Object.keys($scope.data.country[vars.country].type[$scope.input.type].breed[$scope.input.breed].inputWeight));

            // reset all other values
            $scope.feeds.length = 0;
            $scope.input.feed1 = null;
            $scope.input.feed2 = null;
          }
        }
      };

      /*
          get list of feeds from selected input weights
           */
      $scope.getValuesFromInputWeight = function () {

        if ($scope.data) {
          if ($scope.input.inputWeight) {
            $scope.feeds.length = 0;
            animalsSvc.copyArray($scope.feeds, Object.keys($scope.data.country[vars.country].type[$scope.input.type].breed[$scope.input.breed].inputWeight[$scope.input.inputWeight].feed));

            // reset all other values
            $scope.input.feed1 = null;
            $scope.input.feed2 = null;
          }
        }
      };

      /*
          get feed params from selected feed 1
           */
      $scope.getParamsFromInputFeed1 = function () {

        if ($scope.data) {
          if ($scope.input.feed1) {
            var params = $scope.data.country[vars.country].type[$scope.input.type].breed[$scope.input.breed].inputWeight[$scope.input.inputWeight].feed[$scope.input.feed1];

            $scope.params.feed1 = initFeed(params);
          }

          if ($scope.input.feed1 == "aliment complet") {
            $scope.feed2Filter = "aliment complet";
          } else {
            $scope.feed2Filter = undefined;
          }
        }

        $scope.getParamsFromInputAdditive1();
      };

      function initFeed(params) {
        if (vars.country === "France") {
          return {
            cornSilage: [params.ensilage_price, params.ensilage || 0],
            cereal: [params.cereals_price, params.cereals || 0],
            aliment36: [params.aliment_36_price, params.aliment_36 || 0],
            aliment25: [params.aliment_25_price, params.aliment_25 || 0],
            alimentComplete: [params.aliment_complete_price, params.aliment_complete || 0],
            forage: [params.forrage_price, params.forrage || 0],
            additive: [0, null],
            sanitaryPeriod: 20
          };
        } else if (vars.country === "Bresil") { // divide 1000 cause we want price per tonnes
          console.log('quantity nucleo_grao_inteiro : ', params.nucleo_grao_inteiro);
          console.log('quantity alimento_volumoso : ', params.alimento_volumoso);
          console.log('quantity racao_concentra : ', params.racao_concentra);
          console.log('quantity milho_grao_inteiro : ', params.milho_grao_inteiro);
          return {
            nucleoGraoInteiro: [params.nucleo_grao_inteiro_price, params.nucleo_grao_inteiro || 0],
            milhoGraoInteiro: [params.milho_grao_inteiro_price, params.milho_grao_inteiro || 0],
            racaoConcentra: [params.racao_concentra_price, params.racao_concentra || 0],
            alimentoVolumoso: [params.alimento_volumoso_price, params.alimento_volumoso || 0],
            additive: [0, null],
            sanitaryPeriod: 0 // vide sanitaire = 0 pour Bresil
          };
        }
      }


      /**
       * Get production cost
       *
       * @param params datas of json's country
       * @param fatteningDays number of fattening days
       * @param additiveDuration duration of additive
       * @returns {number} feed price per day
       */
      function getProductionCost(params, fatteningDays, additiveDuration) {
        console.log('fatteningDays : ' , fatteningDays);
        console.log('additiveDuration : ' , additiveDuration);
        var allRationsArray = [];
        if (vars.country === "France") {
          allRationsArray = getAllCostRationsFrance(params);
        } else {
          allRationsArray = getAllCostRationsBresil(params);
        }

        console.log('allRationsArray : ' , allRationsArray);

        var costTotalRationsPerDay = 0;
        for (var i = 0; i < allRationsArray.length; i++) {
          costTotalRationsPerDay += allRationsArray[i];
        }

        var additiveDailyCost = toFloat(params.additive[0]);
        var additiveTotalCost = additiveDuration * additiveDailyCost;

        console.log('costTotalRations : ', costTotalRationsPerDay);
        console.log('additiveDailyCost : ', additiveDailyCost);
        console.log('additiveTotalCost : ' , additiveTotalCost);
        return ((costTotalRationsPerDay  + (additiveTotalCost / fatteningDays))* fatteningDays).toFixed(0);
      }

      /**
       * Get all cost of rations for France : sum of quantity * price of all foods
       */
      function getAllCostRationsFrance(params) {
        var allCostRations = [];
        var corn = (toFloat(params.cornSilage[0] / 1000) * toFloat(params.cornSilage[1]));
        var cereals = (toFloat(params.cereal[0] / 1000) * toFloat(params.cereal[1]));
        var aliment36 = (toFloat(params.aliment36[0] / 1000) * toFloat(params.aliment36[1]));
        var aliment25 = (toFloat(params.aliment25[0] / 1000) * toFloat(params.aliment25[1]));
        var forage = (toFloat(params.forage[0] / 1000) * toFloat(params.forage[1]));
        var alimentComplet = (toFloat(params.alimentComplete[0] / 1000) * toFloat(params.alimentComplete[1]));

        allCostRations.push(corn);
        allCostRations.push(cereals);
        allCostRations.push(aliment36);
        allCostRations.push(aliment25);
        allCostRations.push(forage);
        allCostRations.push(alimentComplet);
        return allCostRations;
      }

      /**
       * Get all cost of rations for Bresil : sum of quantity * price of all foods
       */
      function getAllCostRationsBresil(params) {
        var allCostRations = [];
        var nucleoGraoInteiro = (toFloat(params.nucleoGraoInteiro[0] / 1000) * toFloat(params.nucleoGraoInteiro[1]));
        var milhoGraoInteiro = (toFloat(params.milhoGraoInteiro[0] / 1000) * toFloat(params.milhoGraoInteiro[1]));
        var racaoConcentra = (toFloat(params.racaoConcentra[0] / 1000) * toFloat(params.racaoConcentra[1]));
        var alimentoVolumoso = (toFloat(params.alimentoVolumoso[0] / 1000) * toFloat(params.alimentoVolumoso[1]));

        allCostRations.push(nucleoGraoInteiro);
        allCostRations.push(milhoGraoInteiro);
        allCostRations.push(racaoConcentra);
        allCostRations.push(alimentoVolumoso);
        return allCostRations;
      }

      /*
          get additive params from selected additive 1
           */
      $scope.getParamsFromInputAdditive1 = function () {
        if ($scope.data) {
          if ($scope.input.additive1 && $scope.input.additive1 != vars.text.noAdditive) {
            $scope.params.feed1.additive = [toFloat($scope.data.country[vars.country].additives.type[$scope.input.additive1].daily_cost), null];
          }
        }
      };

      /*
          get feed params from selected feed 2
           */
      $scope.getParamsFromInputFeed2 = function () {

        if ($scope.data) {
          if ($scope.input.feed2) {
            var params = $scope.data.country[vars.country].type[$scope.input.type].breed[$scope.input.breed].inputWeight[$scope.input.inputWeight].feed[$scope.input.feed2];

            $scope.params.feed2 = initFeed(params);
          }
        }

        $scope.getParamsFromInputAdditive2();
      };

      /*
      get additive params from selected additive 2
       */
      $scope.getParamsFromInputAdditive2 = function () {

        if ($scope.data) {
          if ($scope.input.additive2 && $scope.input.additive2 != vars.text.noAdditive) {
            $scope.params.feed2.additive = [toFloat($scope.data.country[vars.country].additives.type[$scope.input.additive2].daily_cost), null];
          }
        }
      };

      /*
          convert a string float ("12.23" or "12,23") to a float type
          */

      function toFloat(value) {
        if (typeof value == "string") {
          return parseFloat(value.replace(",", "."));
        } else {
          return value;
        }
      }

      $scope.toFloat = function (value) {
        return toFloat(value);
      };

      /*
          compute the comparison results for the selected feeds and additives
          */
      $scope.compare = function () {
        $scope.control.spinner = true;

        var idSimulationGoogleAnalytics = '_' + Math.random().toString(36).substr(2, 9);

        if (window.cordova) {
          console.log('tracking event');
          window.ga.trackEvent('Type d\'animal', idSimulationGoogleAnalytics, $scope.input.type);
          window.ga.trackEvent('Race', idSimulationGoogleAnalytics, $scope.input.breed);
          window.ga.trackEvent('Poids d\'entree', idSimulationGoogleAnalytics, $scope.input.inputWeight);
          window.ga.trackEvent('Prix d\achat', idSimulationGoogleAnalytics, $scope.input.buyingPrice);
          window.ga.trackEvent('Prix de vente', idSimulationGoogleAnalytics, $scope.input.sellingPrice);
          window.ga.trackEvent('Type de prix', idSimulationGoogleAnalytics, $scope.input.sellingPrice ? 'Kg de carcasse' : 'Kg de poids vif');
          window.ga.trackEvent('Ration 1', idSimulationGoogleAnalytics, $scope.input.feed1);
          window.ga.trackEvent('Ration 2', idSimulationGoogleAnalytics, $scope.input.feed2);
          window.ga.trackEvent('Additif 1', idSimulationGoogleAnalytics, $scope.input.additive1);
          window.ga.trackEvent('Additif 2', idSimulationGoogleAnalytics, $scope.input.additive2);
        } else {
          console.error('cannot track event, no cordova device');
        }

        // get feed params
        var params1 = $scope.params.feed1;
        var params2 = $scope.params.feed2;

        // get feed object of selected feed
        var feed1 = $scope.data.country[vars.country].type[$scope.input.type].breed[$scope.input.breed].inputWeight[$scope.input.inputWeight].feed[$scope.input.feed1];
        var feed2 = $scope.data.country[vars.country].type[$scope.input.type].breed[$scope.input.breed].inputWeight[$scope.input.inputWeight].feed[$scope.input.feed2];

        // get additive object of selected additive
        var additive1 = $scope.input.additive1 ? $scope.data.country[vars.country].additives.type[$scope.input.additive1] : null;
        var additive2 = $scope.input.additive2 ? $scope.data.country[vars.country].additives.type[$scope.input.additive2] : null;

        console.log('prix au kg : ' + $scope.input.sellingPrice);
        console.log('prix au kg de : ' + $scope.input.priceType1 ? 'carcasse' : 'poids vif');
        console.log('poids d entree : ' + $scope.input.inputWeight);
        console.log('prix d achat : ' + $scope.input.buyingPrice);

        // compute results values for feed 1
        var additive1Duration = additive1 ? feed1.fatteningDays * toFloat(additive1.duration) / 100 : 0;
        var additive1Gain = additive1Duration ? (additive1Duration * additive1.ADG / 1000) : 0;
        $scope.comp.additive1Gain = additive1Gain;
        var outputWeight1 = toFloat($scope.input.inputWeight) + (feed1.fatteningDays * feed1.ADG / 1000) + additive1Gain;
        var sellingWeight1 = outputWeight1 * feed1.efficiency_carcass / 100;

        // in case of bresil gmq is ~1 and for france its ~1k
        // more over in bresil version unit is (@) you need to divide by 15
        if (!$scope.caseFrance()) {
          outputWeight1 = parseInt(toFloat($scope.input.inputWeight))
            + parseInt(feed1.fatteningDays * feed1.ADG) + additive1Gain;

          sellingWeight1 = outputWeight1 * feed1.efficiency_carcass / 100 / 15;
        }

        var sellingPrice1 = $scope.input.priceType1 ? toFloat($scope.input.sellingPrice) * sellingWeight1 * feed1.coef_prix_carcasse
            : toFloat($scope.input.sellingPrice) * outputWeight1 * feed1.coef_prix_carcasse;

        $scope.comp.feed1Price = getProductionCost(params1, feed1.fatteningDays, additive1Duration);
        $scope.comp.feed1Profit = (sellingPrice1 - toFloat($scope.input.buyingPrice) - $scope.comp.feed1Price).toFixed(0);
        $scope.comp.fedPerPlace1 = (365 / (feed1.fatteningDays + toFloat(params1.sanitaryPeriod))).toFixed(1);//((365 - toFloat(params1.sanitaryPeriod)) / feed1.fatteningDays).toFixed(1);

        $scope.comp.placeProfit1 = ($scope.comp.fedPerPlace1 * $scope.comp.feed1Profit).toFixed(0);


        // console.log('Ici Marge sur cout : ', $scope.comp.feed1Profit);
        // console.log('Ici animaux engraissés : ', $scope.comp.fedPerPlace1);
        // console.log('Ici Marge à la place : ', $scope.comp.placeProfit1);

        $scope.comp.fatteningDay1 = feed1.fatteningDays;

        console.log('---------- Cas 1ere colonne --------------');
        console.log('ajout des additifs 1 : ' + additive1Gain);
        console.log('poids de sortie 1 : ' + outputWeight1);
        console.log('poids de vente 1 : ' + sellingWeight1);
        console.log('prix de vente 1 : ' + sellingPrice1);
        console.log('cout de production 1 : ' + $scope.comp.feed1Price);
        console.log('jours engraissement 1 : ' + feed1.fatteningDays);
        console.log('vide sanitaire 1 : ' + params1.sanitaryPeriod);
        console.log('animaux engraissés par an par place 2 : ' + $scope.comp.fedPerPlace1);
        console.log('Marge à la place 1 : ' + $scope.comp.placeProfit1);
        console.log('Marge sur cout 1 : ' + $scope.comp.feed1Profit);

        // compute results values for feed 2
        var additive2Duration = additive2 ? feed2.fatteningDays * toFloat(additive2.duration) / 100 : 0;
        var additive2Gain = additive2Duration ? (additive2Duration * additive2.ADG / 1000) : 0;
        $scope.comp.additive2Gain = additive2Gain;
        var outputWeight2 = toFloat($scope.input.inputWeight) + (feed2.fatteningDays * feed2.ADG / 1000) + additive2Gain;
        var sellingWeight2 = outputWeight2 * feed2.efficiency_carcass / 100;

        // in case of bresil gmq is ~1 and for france its ~1k
        // more over in bresil version unit is (@) you need to divide by 15
        if (!$scope.caseFrance()) {
          outputWeight2 = parseInt(toFloat($scope.input.inputWeight))
            + parseInt(feed2.fatteningDays * feed2.ADG) + additive2Gain;

          sellingWeight2 = outputWeight2 * feed2.efficiency_carcass / 100 / 15;
        }

        var sellingPrice2 = $scope.input.priceType1 ? toFloat($scope.input.sellingPrice) * sellingWeight2 * feed2.coef_prix_carcasse
          : toFloat($scope.input.sellingPrice) * outputWeight2 * feed2.coef_prix_carcasse;

        $scope.comp.feed2Price = getProductionCost(params2, feed2.fatteningDays, additive2Duration);
        $scope.comp.feed2Profit = (sellingPrice2 - toFloat($scope.input.buyingPrice) - $scope.comp.feed2Price).toFixed(0);
        $scope.comp.fedPerPlace2 = (365 / (feed2.fatteningDays + toFloat(params2.sanitaryPeriod))).toFixed(1); //((365 - toFloat(params2.sanitaryPeriod)) / feed2.fatteningDays).toFixed(1);

        $scope.comp.placeProfit2 = ($scope.comp.fedPerPlace2 * $scope.comp.feed2Profit).toFixed(0);
        $scope.comp.fatteningDay2 = feed2.fatteningDays;

        console.log('---------- Cas 2eme colonne --------------');
        console.log('ajout des additifs 2 : ' + additive2Gain);
        // console.log('feed2.ADG : ' + feed2.ADG);
        console.log('poids de sortie 2 : ' + outputWeight2);
        console.log('poids de vente 2 : ' + sellingWeight2);
        console.log('prix de vente 2 : ' + sellingPrice2);
        console.log('cout de production 2 : ' + $scope.comp.feed2Price);
        console.log('jours engraissement 2 : ' + $scope.comp.fatteningDay2);
        console.log('vide sanitaire 2 : ' + params2.sanitaryPeriod);
        console.log('animaux engraissés par an par place 2 : ' + $scope.comp.fedPerPlace2);
        console.log('Marge à la place 2 : ' + $scope.comp.placeProfit2);
        console.log('Marge sur cout 2 : ' + $scope.comp.feed2Profit);

        // compute profit values
        $scope.comp.feedProfit = ($scope.comp.feed2Profit - $scope.comp.feed1Profit).toFixed(0);

        console.log('Marge sur cout alimentaire : ' + $scope.comp.feedProfit);

        $scope.comp.fatteningDayReduction = $scope.comp.fatteningDay2 - $scope.comp.fatteningDay1;
        $scope.control.spinner = false;
      };

      $scope.recalculate = function () {
        $scope.compare();

        //scroll to the top of the page to see new results of comparison
        $ionicScrollDelegate.scrollTop(true);
      };

      $scope.toggleParams = function () {
        $scope.control.paramPanel = !$scope.control.paramPanel;
        // scroll up few lines to let user see beginning of params table
        // $ionicScrollDelegate.scrollBy(0, 100, true);
      };

      $scope.toggleDetailResultPanel = function () {
        $scope.control.detailResultPanel = !$scope.control.detailResultPanel;
      };

      $scope.compEnabled = function () {
        return $scope.input.type && $scope.input.breed && $scope.input.inputWeight && $scope.input.buyingPrice && $scope.input.sellingPrice && $scope.input.feed1 && $scope.input.feed2;
      };

      $scope.checkbox1Change = function () {
        $scope.input.priceType2 = !$scope.input.priceType1;
      };

      $scope.checkbox2Change = function () {
        $scope.input.priceType1 = !$scope.input.priceType2;
      };

      $scope.closeKeyboard = function (event) {
        if (event.charCode == 13) {
          if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.close();
          }
        }
      };

      $scope.goBack = function () {
        if ($ionicHistory.backView()) {
          $ionicHistory.goBack();
        } else {
          $location.path('/login');
        }
      };

      $scope.validSettings = function () {
        $scope.setLanguage();
        $scope.setDataFromLanguage();

        vars.settings.language = $scope.input.language;
        animalsSvc.saveSettings();
        $scope.goBack();
      };
    })
})();
