// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('standiBovin', ['ionic', 'auth0.auth0', 'angular-jwt', 'angular-google-analytics'])

  .run(run)
  .config(config);

run.$inject = ['$ionicPlatform', 'authService'];

function run($ionicPlatform, authService) {
  $ionicPlatform.ready(function () {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }

    // Use the authManager from angular-jwt to check for
    // the user's authentication state when the page is
    // refreshed and maintain authentication
    authService.checkAuthOnRefresh();

    // Process the auth token if it exists and fetch the profile
    authService.authenticateAndGetProfile();
    window.ga.startTrackerWithId('UA-101532241-1', 30);
    // prod UA-101532241-1
    // dev UA-106982852-1
  });
}

config.$inject = ['$stateProvider', '$urlRouterProvider', 'angularAuth0Provider', 'AnalyticsProvider'];

function config($stateProvider, $urlRouterProvider, angularAuth0Provider, AnalyticsProvider) {

  console.log('app config');
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    .state('login', {
      url: "/login",
      templateUrl: 'template/login.html'
    })
    .state('main', {
      url: "/",
      templateUrl: 'template/main.html',
      data: {
        securedFeature: true
      }
    })
    .state('results', {
      url: "/results",
      templateUrl: 'template/results.html',
      data: {
        securedFeature: true
      }
    })
    .state('settings', {
      url: "/settings",
      templateUrl: 'template/settings.html'
    })
    .state('faq', {
      url: "/faq",
      templateUrl: 'template/faq.html'
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

  // Initialization for the angular-auth0 library
  angularAuth0Provider.init({
    clientID: AUTH0_CLIENT_ID,
    domain: AUTH0_DOMAIN
  });

}

