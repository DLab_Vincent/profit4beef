(function () {
  angular.module('standiBovin')

    .factory('animalsSvc', function ($http) {

      var vars = {
        country: null,
        systemLanguage: "fr-FR",
        languages: [],
        settings: {language: null},
        text: {},
        data: {},
        values: {
          types: [],
          breeds: [],
          inputWeights: [],
          feeds: [],
          additives: []
        },
        input: {},
        comp: {},
        control: {}
      };

      function getSettings() {

        //localStorage.removeItem('profit4beef');

        var userCookies = JSON.parse(localStorage.getItem('profit4beef')) || {};
        if (userCookies.language) {
          copyObject(vars.settings, userCookies);
        } else {
          // get default language from system
          vars.systemLanguage = window.navigator.languages ? window.navigator.languages[0] : (window.navigator.language || window.navigator.userLanguage);

          // set app language
          if (vars.systemLanguage && !vars.settings.language) {
            if (vars.systemLanguage.indexOf("fr") == 0) {
              vars.settings.language = "Français";
              // vars.country = 'France';
            }
            /*else if (vars.systemLanguage.indexOf("en") == 0) {
              vars.settings.language = "English";
            } else if (vars.systemLanguage.indexOf("es") == 0) {
              vars.settings.language = "Español";
            } */
            else if (vars.systemLanguage.indexOf("pt") == 0) {
              vars.settings.language = "Bresil";
              // vars.country = "Bresil";
              // console.log('vars.systemLanguage.indexOf("pt") OK');
            } else {
              // console.log('vars.systemLanguage finds nothing francais default');
              vars.settings.language = "Français";
            }
          }
        }

        // get available languages files for language settings list
        $http.head('data/lang_french.json')
          .then(function success() {
            vars.languages.push("Français");
          });

        $http.head('data/lang_english.json')
          .then(function success() {
            vars.languages.push("English");
          });

        $http.head('data/lang_spanish.json')
          .then(function success() {
            vars.languages.push("Español");
          });

        $http.head('data/lang_portuguese.json')
          .then(function success() {
            vars.languages.push("Bresil");
          });
      }

      function saveSettings() {
        localStorage.setItem('profit4beef', JSON.stringify(vars.settings));
      }

      function initVars() {
        // clear data
        for (var member in vars.data) delete vars.data[member];

        // clear values lists
        vars.values.types.length = 0;
        vars.values.breeds.length = 0;
        vars.values.inputWeights.length = 0;
        vars.values.feeds.length = 0;
        vars.values.additives.length = 0;

        // clear angular values
        vars.input.type = null;
        vars.input.breed = null;
        vars.input.inputWeight = null;
        vars.input.buyingPrice = null;
        vars.input.sellingPrice = null;
        vars.input.priceType1 = true;
        vars.input.priceType2 = false;
        vars.input.feed1 = null;
        vars.input.feed2 = null;
        vars.input.additive1 = null;
        vars.input.additive2 = null;

        vars.comp.feed1Price = null;
        vars.comp.feed2Price = null;
        vars.comp.feed1Profit = null;
        vars.comp.feed2Profit = null;
        vars.comp.feedProfitReduction = null;
        vars.comp.fedPerPlace1 = null;
        vars.comp.fedPerPlace2 = null;
        vars.comp.placeProfit1 = null;
        vars.comp.placeProfit2 = null;
        vars.comp.fatteningDay1 = null;
        vars.comp.fatteningDay2 = null;
        vars.comp.fatteningDayReduction = null;

        vars.control.paramPanel = false;
        vars.control.detailResultPanel = false;
        vars.control.spinner = false;
      }

      function copyArray(dest, src) {
        dest = src.reduce(function (coll, item) {
          coll.push(item);
          return coll;
        }, dest);
      }

      function copyObject(dest, src) {
        Object.keys(src).forEach(function (key) {
          dest[key] = src[key];
        });
      }

      // function getData (key) {
      // 	if (key && vars.country) {
      // 		$http.get('data/bovin_' + vars.country + '.json')
      // 		.then (function(res) {
      // 			// fill data object, do not change its reference
      // 			copyObject(vars.data, res.data);

      // 			// get json data for selected country
      // 			if (vars.data) {
      // 				// set list of bovine types. Copy items into arrays.
      // 				vars.values.types.length = 0;
      // 				if (vars.data['country'][vars.country]) {
      // 					copyArray (vars.values.types, Object.keys(vars.data['country'][vars.country]['type']));

      // 					// get list of additives
      // 					vars.values.additives.length = 0;
      // 					copyArray (vars.values.additives, Object.keys(vars.data['country'][vars.country]['additives']['type']));
      // 					vars.values.additives.splice(0, 0, vars.text.noAdditive);
      // 				}
      // 			}
      // 		});
      // 	}
      // };

      function getData(key) {
        console.log('getData');
        if (key && vars.country) {
          console.log('key && country');
          // console.log('key : ', key);
          console.log('vars.country : ', vars.country);

          $http.get('data/bovin_' + vars.country + '.json.aes')
            .then(function (res) {
              var data = CryptoJS.AES.decrypt(res.data, key);
              data = JSON.parse(data.toString(CryptoJS.enc.Utf8));

              // fill data object, do not change its reference
              copyObject(vars.data, data);

              // get json data for selected country
              if (vars.data) {
                // set list of bovine types. Copy items into arrays.
                vars.values.types.length = 0;
                if (vars.data.country[vars.country]) {
                  copyArray(vars.values.types, Object.keys(vars.data.country[vars.country].type));

                  // get list of additives
                  vars.values.additives.length = 0;
                  console.log('vars.data.country[vars.country] : ', vars.data.country[vars.country]);
                  console.log('vars.data.country[vars.country].additives : ', vars.data.country[vars.country].additives);
                  copyArray(vars.values.additives, Object.keys(vars.data.country[vars.country].additives.type));
                  vars.values.additives.splice(0, 0, vars.text.noAdditive);
                }
              }
            });
        }
      }

      initVars();
      getSettings();

      return {
        getData: getData,
        text: function (language) {
          switch (language) {
            case "Français":
              return $http.get('data/lang_french.json');
            case "English":
              return $http.get('data/lang_english.json');
            case "Español":
              return $http.get('data/lang_spanish.json');
            case "Bresil":
              return $http.get('data/lang_portuguese.json');
            default :
              return $http.get('data/lang_french.json');
          }
        },
        vars: vars,
        clear: function () {
          initVars();
        },
        copyArray: copyArray,
        copyObject: copyObject,
        saveSettings: saveSettings
      };
    })
})();
