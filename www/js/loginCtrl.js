(function () {

  angular.module('standiBovin')

    .controller('LoginController', LoginController);

  LoginController.$inject = ['$state', '$ionicHistory', '$ionicPopup', '$http', 'authService', 'animalsSvc'];

  function LoginController($state, $ionicHistory, $ionicPopup, $http, authService, animalsSvc) {
    var vm = this;

    vm.cred = authService.cred;
    vm.login = login;
    vm.logout = logout;
    vm.resetPassword = resetPassword;
    vm.signup = signup;
    vm.loginWithGoogle = authService.loginWithGoogle;

    vm.isLoggedIn = authService.isLoggedIn;

    // Log in with username and password
    function login() {
      authService.login(vm.cred.username, vm.cred.password);
      $ionicHistory.clearHistory();
    }

    // Log in with username and password
    function logout() {
      authService.logout();
    }

    // Log in with username and password
    function resetPassword() {
      // if (!vm.cred.username) {
      //     return $ionicPopup.alert({
      //     title: 'invalid email !',
      //     template: ""
      //   });
      // }
      // authService.changePassword(vm.cred.username);


      $http.post('https://' + AUTH0_DOMAIN + '/dbconnections/change_password',
        {
          client_id: AUTH0_CLIENT_ID,
          email: vm.cred.username,
          connection: AUTH0_CONNECTION //'Username-Password-Authentication'
        })
        .then(function success(response) {
          return $ionicPopup.alert({
            title: 'Password change request',
            template: response.data
          });
        }, function error(response) {
          console.log('response : ', response);
          return $ionicPopup.alert({
            title: 'Error',
            template: response.data.error
          });
        });
    }

    function signup() {
      authService.signup(vm.cred.username, vm.cred.password);
    }
  }
})();
