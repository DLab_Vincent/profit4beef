# Profit4Beef

### Description

Profit4Beef est une application proposée par Evialis, filiale de Neovia.

Elle est développée avec Ionic1 et AngularJS et a pour but d'être un outil 
de comparaison rapide d'efficacité économique de rations alimentaires.

L'application est actuellement fonctionnelle en version Française et Bresilienne.

Le dépôt git distant est : 

    https://bitbucket.org/DLab_Vincent/standibovin

### Développement

Le projet a été développé en partie grâce à l’IDE Webstorm de Jetbrains : https://www.jetbrains.com/webstorm/

### Fonctionnalités

Profit4Beef a deux interfaces graphiques principales. 

Une qui permet de saisir des données en entrée comme :
* le type d'animal, 
* la race le poids d'entrée, 
* le prix d'achat de l'animal, 
* le prix de vente soit au kg de carcasse, soit au kg de poids vif, dans le cas de la France,
* deux rations alimentaires
* optionnelement des additifs

Une qui permet de visualiser le résultat de la simulation et éventuellement de modifier des paramètres.

Visualisation de : 
* la marge sur coût alimentaire
* jours d'engraissements
* prix de ration par vaches, 
* nombre d'animaux engraissés par an
* marge à la place par an

Edition possible pour chaque alimentation de :
* prix à la tonne
* kg distribués par jours
* et du vide sanitaire en jours

### Composition du projet

Tout ce qui est dans www est dans l'application finale 

* Le répertoire utils n'est pas un dossier de l'application mais du projet.
On y trouve les données temporaires comme les fichiers de données pour chaque pays, le script pour chiffrer ces données, 
les scripts pour formatter les données métier.
  
* [www/css](www/css) où se trouve le fichier style.css contenant tout le style de l'application.

* [www/data](www/data) où se trouvent les fichiers de traductions (lang_french.json, lang_portuguese.json), 
le fichier auth0.variables.js contenant les informations necessaires pour le service d'authentification, 
les fichiers xx.json.aes chiffrées contenant les données spécifiques au métier et au pays.

* [www/img](www/img) le répertoire où on trouve les images utiles au projet, entre autre l'image du header.

* [www/js](www/js) le répertoire où l'on trouve les modules, services et controllers AngularJS de l'application.

* [www/template](www/template) le répertoire où l'on trouve les interfaces au format html

Dans config.xml on trouvera, entre autre, le nom de package (Android) ou identifiant de lot (ios) 
et la version de l'application. 

## Ajout d'une page

Dans www/js/app.js le $stateProvider gère la navigation. Ajouter un suffixe url associé à son template.
Ensuite soit utiliser le main controlleur, soit créer un controlleur spécifique à la page.

## Ajouter un pays

Dans Profit4Beef, ajouter un pays consiste à ajouter les traductions propres à ce nouveau pays, 
mais aussi ses données métiers.

#### Langue

Créer un fichier lang_xx.json dans le répertoire [data](www/data) avec les mêmes données que dans [lang_french.json](www/data/lang_french.json).
Tout a besoin d'être traduit sauf les données spécifiques au pays comme fourrage grossier, aliment 36%, 
ensillage maïs pour la France ou Concentrado Alto Grão, Milho grão inteiro pour le Bresil.

Dans le service principal, i.e [mainScv.js](www/js/mainSvc.js), ajouter la nouvelle disponible à la liste des langues disponibles comme ceci : 
    
    $http.head('data/lang_french.json')
        .then(function success() {
          vars.languages.push("Français");
        });
   
Pour que les traductions dans le json de langues soit utilisées,
toujours dans le service principal, ajouter dans text: function (language) {...} 
un cas dans le switch en pointant vers le nouveau fichier : 

    case "Français":
        return $http.get('data/lang_french.json');

Ajouter dans [results.html](www/template/results.html) le conteneur du nouveau pays : 
    
    <div ng-if="caseFrance()"> ....</div>
    <div ng-if="case***()"> ....</div>

Dans le conteneur nouvellement crée, ajouter la ligne pour chaque rations : 
    
    <div class="row row-custom">
      <div class="col col-50">
        {{text.nucleoGraoInteiro}}
        <div class="item-note">
          {{text.pricePerTonne}}
          {{text.paramsUnit2}}
        </div>
      </div>
      <div class="col col-custom">
          <input type="number" ng-model="params.feed1.nucleoGraoInteiro[0]"
                 ng-keypress="closeKeyboard($event)">
          <input type="number"
                 ng-model="params.feed1.nucleoGraoInteiro[1]"
                 ng-keypress="closeKeyboard($event)">
      </div>
      <div class="col">
          <input type="number" ng-model="params.feed2.nucleoGraoInteiro[0]"
                 ng-keypress="closeKeyboard($event)">
          <input type="number"
                 ng-model="params.feed2.nucleoGraoInteiro[1]"
                 ng-keypress="closeKeyboard($event)">
      </div>
    </div>
  
Ici on crée une ligne dans un tableau à trois colonnes. 
La première colonne correspond à une nourriture
* *{{text.nucleoGraoInteiro}}* est la traduction de nucleoGraoInteiro
* *{{text.pricePerTonne}}* est la traduction de prix à la tonne
* *{{text.paramsUnit2}}* est la traduction de Kg bruts distribués/j

La deuxième colonne correspond à la nourriture fournie pour la ration 1
* *ng-model="params.feed1.nucleoGraoInteiro[0]"* est le prix à la tonne modifiable de nucleoGraoInteiro de la ration 1
* *ng-model="params.feed1.nucleoGraoInteiro[1]"* est la quantité en kg distribuée de nucleoGraoInteiro  de la ration 1

La troisième colonne correspond à la nourriture fournie pour la ration 2
* *ng-model="params.feed2.nucleoGraoInteiro[0]"* est le prix à la tonne modifiable de nucleoGraoInteiro de la ration 2
* *ng-model="params.feed2.nucleoGraoInteiro[1]"* est la quantité en kg distribuée de nucleoGraoInteiro  de la ration 2


#### Donnees

Le nouveau pays crée a ses propres données métiers (animaux, races, poids d'entrés, additifs, etc).

Il faut d'abord avoir un fichier json formaté. 

Copier les colonnes type, race, poids entree, ration, nb_jours_engraissement, GMQ, rdt, coef_prix_carcasse
et les quantités et prix des rations alimentaires

Coller ces colonnes dans un fichier txt

* Concatener tous les noms composés avec un '-' : e.g Macho inteiro devient Macho-inteiro, aliment complet devient aliment-complet
* Remplacer tous les accents et caractères spécifiques : ç -> c, e.g á -> a, é -> e, ã -> a, í -> i, etc
* Remplacer toutes les virgules par des points
* Remplacer les espaces par des virgules
* Copier cette ligne en première ligne du fichier : /!\ s'assurer du bon ordre des colonnes

      type, race, poids entree, ration, nb_jours_engraissement, GMQ, rdt, coef_prix_carcasse,

* Si on est sur la version Française : copier ceci à la suite de la première du fichier texte  : 

      mais, cereales, aliment36, aliment25, complet, fourrage

* Si on est sur la version Bresilienne : copier ceci à la suite de la première du fichier texte : 

      nucleo_grao_inteiro, milho_grao_inteiro, racao_concentra, alimento_volumoso
      
Copier le fichier texte, aller sur [csvjson](http://www.csvjson.com/csv2json)
Coller le fichier dans la colonne de gauche, cliquer sur convert, récupérer le json

Créer un fichier br_datexxx.json dans le répertorie [utils/scripts](utils/scripts)
Dans [utils/scripts/json_create_france.py](utils/scripts/json_create_france.py) ou 
[utils/scripts/json_create_bresil.py](utils/scripts/json_create_bresil.py)
changer à la ligne 3 le nom du fichier lu

      fd = open("br_datexxx.json", "r")

Se placer dans le répertoire du script 

      cd utils\scripts
      
Puis exécuter le script pour avoir les données formattées
      
Si la version est Bresilienne : 

      python json_create_bresil.py > br_datexxx_converted.json
      
Si la version est Française : 

    python json_create_france.py > fr_datexxx_converted.json      

Dans le fichier nouvellement crée utils\scripts\br_datexxx_converted.json

* Remplacer les noms concaténés avec un '-' : e.g Macho-inteiro devient Macho inteiro, aliment-complet devient aliment complet
* Remettre les accents et caractères spécifiques : e.g c -> ç, a -> á, e -> é, a -> ã, i -> í, etc
* Remplacer toutes les points par des virgules s'il le faut

Le format final a besoin pour chaque type d'animal, pour chaque race, pour chaque poids d'entrée, pour chaque ration : 
* un ADG (clé ADG), 
* un nombre de jours d'engraissements (clé fatteningDays)
* le nombre de kg distribués par jours (valeur) avec en clé le nom de la ration

Par exemple une partie du json formatté du bresil est comme cela : 

    "type": {
            "Macho Inteiro": {
              "breed": {
                "F1 Racas europeias  x Nelore": {
                  "inputWeight": {
                    "300": {
                      "feed": {
                        "Dieta grao inteiro": {
                          "ADG": 1.75,
                          "concentrado_alto_grao": 1.12,
                          "fatteningDays": 55,
                          "milho_grao_inteiro": 6.35,
                          "pasto_verde": "",
                          "racao_pronta": "",
                          "volumoso": ""
                        }
                        ....
      
      
* A la fin du json, ajouter les additifs du pays avec pour chaque type d'additif un ADG, 
une durée (clé duration) et un coût de maintenance (clé daily_cost)

Il n'y a besoin d'aucune autre manipulation pour que les additifs soient pris en compte.

Une fois que ce fichier .json est crée, il doit être chiffré avec AES256. 
Le fichier est déchiffré en mémoire à l'authentification de l'utilisateur, les données ne sont pas stockées autrement.

Pour chiffrer ce fichier, aller dans [utils/scripts/crypt.js](utils/scripts/crypt.js), modifier le fichier lu et sauvegardé par ce que vous voulez chiffrer.
    
    fs.readFile("utils/br_datexxx_converted.json"
    
Si version Française : 
    
    fs.writeFile('www/data/bovin_France.json.aes', ...

Si version Bresilienne : 

    fs.writeFile('www/data/bovin_Bresil.json.aes', ...
    
Noter que le json avec les données métier en clair est dans le répertoire utils qui n'est pas dans l'application.
Le fichier chiffré est lui dans data qui est un répertoire de l'application.

Ensuite, executer ce script pour chiffrer le json : 
    
    cd <git repo>
    node utils/scripts/crypt_json.js


Les nouvelles données sont intégrées dans l'application ...

#### Ajouter des rations alimentaires à un pays 

Pour initialiser la ration spécifique au pays, on crée l'objet feed dans la fonction initFeed(params) de mainSvc.js.
On doit retrouver les informations suivantes : les rations, les additifs, le vide sanitaire. 
Les rations sont représentées par des tableaux de deux éléments : le prix de la ration (1er element) 
et la quantité distribuée en kg (2eme element).

Attention les unités sont importantes, le prix des rations est en prix à la tonne et la quantité doit être en kg.
Ces données sont modifiables dans par l'utilisateur dans les paramètres de la page resultat.
Par exemple pour la France, l'objet feed est : 
   
    return {
                cornSilage: [params.ensilage_price, params.ensilage || 0],
                cereal: [params.cereals_price, params.cereals || 0],
                aliment36: [params.aliment_36_price, params.aliment_36 || 0],
                aliment25: [params.aliment_25_price, params.aliment_25 || 0],
                alimentComplete: [params.aliment_complete_price, params.aliment_complete || 0],
                forage: [params.forrage_price, params.forrage || 0],
                additive: [0, null],
                sanitaryPeriod: 20
              };

Le sanitaryPeriod est determiné par le métier et est spécifique au pays.

Noter que params est issu du json formaté par le script et correspond à la ration à comparer (ration 1 ou 2) 
et a ce format :
    
    "aliment complet": {
      "ADG": 0,
      "coef_prix_carcasse": 0,
      "fatteningDays": 0,
      "ensilage_price": 0,
      "ensilage": 0,
      "cereals_price": 0,
      "cereals": 0,
      "aliment_36_price": 0,
      "aliment_36": 0,
      "aliment_25_price": 0,
      "aliment_25": 0,
      "aliment_complete_price": 0,
      "aliment_complete": 0,
      "forrage": 0,
      "forrage_price": 0,
    }

Ensuite, le calcul de la marge sur coût alimentaire doit être adapté.
Il faut prendre en compte les rations spécifiques au pays dans le calcul. 
Pour cela dans la fonction getProductionCost de [www/js/mainCtrl.js](www/js/mainCtrl.js), on ajoute le nouveau pays dans le if else 
et on crée une fonction getAllCostRations_NEW_COUNTRY qui doit retourner
un tableau de float. Ce tableau correspond à tous les coûts de production des rations : quantité au kg * prix au kg.

En France on a
    
    toFloat(params.cornSilage[0] / 1000) * toFloat(params.cornSilage[1]))
    + (toFloat(params.cereal[0] / 1000) * toFloat(params.cereal[1]))
    + etc
    
Au Bresil on a

    (toFloat(params.nucleoGraoInteiro[0] / 1000) * toFloat(params.nucleoGraoInteiro[1]))
    + (toFloat(params.milhoGraoInteiro[0] / 1000) * toFloat(params.milhoGraoInteiro[1]))
    + etc

Noter qu'on divise par 1000 car le calcul est en prix au kg.

## Analytics

Les données entrées manuellement dans la page de simulation sont trackées par Google Analytics. 
Dans [www/js/mainCtrl.js](www/js/mainCtrl.js), pour toutes les entrées on envoie au serveur la donnée avec sa clé (ici Type d'animal avec son type).
On envoie aussi un identifiant la simulation pour pouvoir retrouvée toutes les données propres à une simulation.     
    
    window.ga.trackEvent('Type d\'animal', idSimulationGoogleAnalytics, $scope.input.type);
    window.ga.trackEvent('Race', idSimulationGoogleAnalytics, $scope.input.breed);
    etc
    
idSimulationGoogleAnalytics est une clé de la simulation permettant de retrouver toutes les 
informations spécifiques à une simulation sur le serveur Google analytics 


## Development

#### Android

```
npm run run:android
```

#### IOS

```
npm run run:ios
```

#### Navigator

```
ionic:serve
```

## Production : Build et publication

Avant une mise à jour, augmenter la version de l'application dans config.xml

Pour Android, dans package.json, clique droit sur le script release:android
Entrer le mot de passe du keystore
L'apk de production est ici : platforms/android/build/outputs/apk/android-release.apk
Sur le Playstore, dans gestion de la publication, dans versions de l'application, créer une version en déposant l'apk.

Pour IOS, sur un mac, clique droit sur release:ios
Ouvrir dans le finder platforms/ios
Double cliquer sur profit4Beef.xcarchive
XCode est alors ouvert, on peut faire 'upload to appstore' en selectionnant la team Neovia.
Sur Itunes Connect, ajouter une version ios avec le nom de la version et ajouter le build. 