(function() {
	angular.module('standiBovin')

	.service('authService', authService);

	authService.$inject = ['$rootScope', 'angularAuth0', 'authManager', 'jwtHelper', '$location', '$ionicPopup', 'animalsSvc'];

	function authService($rootScope, angularAuth0, authManager, jwtHelper, $location, $ionicPopup, animalsSvc) {

		var userProfile = JSON.parse(localStorage.getItem('profile')) || {};
		var loggedIn = false;
		var vars = animalsSvc.vars;

		// login credentials
		var cred = {username : null,
					password : null};

		function login(username, password) {
			vars.control.spinner = true;
			angularAuth0.login({
				connection: 'Username-Password-Authentication',
				responseType: 'token',
				popup: true,
				email: username,
				password: password
			}, onAuthenticated, null);
		}

		function signup(username, password) {
			angularAuth0.signup({
				connection: 'Username-Password-Authentication',
				responseType: 'token',
				popup: true,
				email: username,
				password: password
			}, onAuthenticated, null);
		}

		function loginWithGoogle() {
			angularAuth0.login({
				connection: 'google-oauth2',
				responseType: 'token',
				popup: true
			}, onAuthenticated, null);
		}

		// Logging out just requires removing the user's
		// id_token and profile
		function logout() {
			localStorage.removeItem('id_token');
			localStorage.removeItem('profile');
			authManager.unauthenticate();
			userProfile = {};

			$location.path('/login');
			animalsSvc.clear();

			loggedIn = false;
		}

		function authenticateAndGetProfile() {
			var result = angularAuth0.parseHash(window.location.hash);

			if (result && result.idToken) {
				onAuthenticated(null, result);
			} else if (result && result.error) {
				onAuthenticated(result.error);
			}
		}

		function onAuthenticated(error, authResult) {
			if (error) {
				vars.control.spinner = false;
				return $ionicPopup.alert({
					title: 'Login failed!',
					template: error
					});
			}

			localStorage.setItem('id_token', authResult.idToken);
			authManager.authenticate();

			angularAuth0.getProfile(authResult.idToken, function (error, profileData) {
				if (error) {
					return console.log(error);
				}

				// get decryption key in user metadata
				var key = profileData.app_metadata.key ? profileData.app_metadata.key : null;
				vars.country = profileData.app_metadata.country ? profileData.app_metadata.country : null;

				// remove key before storing user profile on local storage
				delete profileData.app_metadata.key;
				localStorage.setItem('profile', JSON.stringify(profileData));
				userProfile = profileData;

				// goto main page
				$location.path('/');

				// decrypt data with key
				if (key) {
					animalsSvc.getData(key);
				}

				vars.control.spinner = false;
				loggedIn = true;
			});
		}

		function checkAuthOnRefresh() {
			var token = localStorage.getItem('id_token');
			if (token) {
				if (!jwtHelper.isTokenExpired(token)) {
					if (!$rootScope.isAuthenticated) {
						authManager.authenticate();

						angularAuth0.getProfile(token, function (error, profileData) {
							if (error) {
								return console.log(error);
							}

							cred.username = profileData.email ? profileData.email : null;
							var key = profileData.app_metadata.key ? profileData.app_metadata.key : null;
							vars.country = profileData.app_metadata.country ? profileData.app_metadata.country : null;
							if (key) {
								animalsSvc.getData(key);
								$location.path('/');
							}

							loggedIn = true;
						});
					}
				}
			}
		}

	    $rootScope.$on('$stateChangeStart', function (event, nextState) {
	        if (nextState.data && nextState.data.securedFeature) {

				var token = localStorage.getItem('id_token');
				if (!token || jwtHelper.isTokenExpired(token)) {
					event.preventDefault();
					$location.path('/login');
				}
	        }
	    });

		function isLoggedIn() {
			return loggedIn;
		}

		return {
			login: login,
			logout: logout,
			signup: signup,
			loginWithGoogle: loginWithGoogle,
			checkAuthOnRefresh: checkAuthOnRefresh,
			authenticateAndGetProfile: authenticateAndGetProfile,
			isLoggedIn: isLoggedIn,
			userProfile : userProfile,
			cred : cred,
		}
	}
})();