(function() {

	angular.module('standiBovin')

	.controller('inputPanelCtrl', function($scope, $ionicScrollDelegate, $ionicHistory, animalsSvc, authService) {

		$scope.location = window.location

		// get default vars from service
		var vars = animalsSvc.vars;

		$scope.data = vars.data;

		// language texts
		$scope.text = vars.text;

		// bovin type
		$scope.types = vars.values.types;

		// bovin breeds
		$scope.breeds = vars.values.breeds;

		// bovin input weight
		$scope.inputWeights = vars.values.inputWeights;

		// bovin feeds
		$scope.feeds = vars.values.feeds;

		// list of additives
		$scope.additives = vars.values.additives;


		// init angular variables

		// selected values
		$scope.input = vars.input;

		// compaison values for result page
		$scope.comp = vars.comp;

		$scope.control = vars.control;

		$scope.params = {"feed1" : {},
		               	 "feed2" : {}};

		$ionicHistory.clearHistory();

		// init language
		$scope.languages = vars.languages;
		$scope.input.language = vars.settings.language;

		/*
		get language texts from selected language
		*/
		$scope.setLanguage = function () {
			animalsSvc.text($scope.input.language).then (function(res) {
				animalsSvc.copyObject(vars.text, res.data);
			}, function (err) {
			});
		}

		$scope.setLanguage();

		$scope.translate = function (text){
			if (typeof text == "string") {
				if ($scope.text[text]) {
					text = $scope.text[text];
				}
			} else if (typeof text == "object") {

			}
		}

		/*
		get list of breeds from selected type
		 */
		$scope.getValuesFromType = function () {

		    if ($scope.data) {
				if ($scope.input.type) {
					$scope.breeds.length = 0;
					animalsSvc.copyArray ($scope.breeds, Object.keys($scope.data.country[vars.country].type[$scope.input.type].breed));

					// reset all other values
					$scope.inputWeights.length = 0;
					$scope.input.inputWeight = null;
					$scope.feeds.length = 0;
					$scope.input.feed1 = null;
					$scope.input.feed2 = null;
				};
			}
		};

		/*
		get list of input weights from selected breed
		 */
		$scope.getValuesFromBreed = function () {

		    if ($scope.data) {
				if ($scope.input.breed) {
					$scope.inputWeights.length = 0;
					animalsSvc.copyArray ($scope.inputWeights, Object.keys($scope.data.country[vars.country].type[$scope.input.type].breed[$scope.input.breed].inputWeight));

					// reset all other values
					$scope.feeds.length = 0;
					$scope.input.feed1 = null;
					$scope.input.feed2 = null;
				};
		    }
		  };

		/*
		get list of feeds from selected input weights
		 */
		$scope.getValuesFromInputWeight = function () {

		    if ($scope.data) {
				if ($scope.input.inputWeight) {
					$scope.feeds.length = 0;
					animalsSvc.copyArray ($scope.feeds, Object.keys($scope.data.country[vars.country].type[$scope.input.type].breed[$scope.input.breed].inputWeight[$scope.input.inputWeight].feed));

					// reset all other values
					$scope.input.feed1 = null;
					$scope.input.feed2 = null;
				};
		    }
		};

		/*
		get feed params from selected feed 1
		 */
		$scope.getParamsFromInputFeed1 = function () {

		    if ($scope.data) {
				if ($scope.input.feed1) {
					var params = $scope.data.country[vars.country].type[$scope.input.type].breed[$scope.input.breed].inputWeight[$scope.input.inputWeight].feed[$scope.input.feed1];

					$scope.params.feed1 = {cornSilage : [params.ensilage_price / 1000, params.ensilage || 0],
					                       cereal : [params.cereals_price / 1000, params.cereals || 0],
					                       aliment36 : [params.aliment_36_price / 1000, params.aliment_36 || 0],
					                       aliment25 : [params.aliment_25_price / 1000, params.aliment_25 || 0],
					                       alimentComplete : [params.aliment_complete_price / 1000, params.aliment_complete || 0],
					                       forage : [params.forrage_price / 1000, params.forrage || 0],
					                       additive : [0, null],
					                       sanitaryPeriod : 20,
					                      };
					};

					if ($scope.input.feed1 == "aliment complet") {
						$scope.feed2Filter = "aliment complet";
					} else {
						$scope.feed2Filter = undefined;
					}
				}
			};

		/*
		get additive params from selected additive 1
		 */
		$scope.getParamsFromInputAdditive1 = function () {

			if ($scope.data) {
				if ($scope.input.additive1 && $scope.input.additive1 != vars.text.noAdditive) {
					$scope.params.feed1.additive = [toFloat($scope.data.country[vars.country].additives.type[$scope.input.additive1].daily_cost), null];
				};
			}
		};

		/*
		get feed params from selected feed 2
		 */
		$scope.getParamsFromInputFeed2 = function () {

			if ($scope.data) {
				if ($scope.input.feed2) {
					var params = $scope.data.country[vars.country].type[$scope.input.type].breed[$scope.input.breed].inputWeight[$scope.input.inputWeight].feed[$scope.input.feed2];

					$scope.params.feed2 = {cornSilage : [params.ensilage_price / 1000, params.ensilage || 0],
					                       cereal : [params.cereals_price / 1000, params.cereals || 0],
					                       aliment36 : [params.aliment_36_price / 1000, params.aliment_36 || 0],
					                       aliment25 : [params.aliment_25_price / 1000, params.aliment_25 || 0],
					                       alimentComplete : [params.aliment_complete_price / 1000, params.aliment_complete || 0],
					                       forage : [params.forrage_price / 1000, params.forrage || 0],
					                       additive : [0, null],
					                       sanitaryPeriod : 20,
					                      };
				};
			}
		};

		/*
		get additive params from selected additive 2
		 */
		$scope.getParamsFromInputAdditive2 = function () {

		    if ($scope.data) {
				if ($scope.input.additive2 && $scope.input.additive2 != vars.text.noAdditive) {
					$scope.params.feed2.additive = [toFloat($scope.data.country[vars.country].additives.type[$scope.input.additive2].daily_cost), null];
				};
		    }
		};

		/*
		convert a string float ("12.23" or "12,23") to a float type
		*/

		function toFloat (value) {
			if (typeof value == "string" && vars.country == 'France') {
				return parseFloat(value.replace(",", ".")); 
			} else {
				return value;
			}
		}

		$scope.toFloat = function (value) {
			return toFloat(value);
		};

		/*iency_carcass'
		compute the comparison results for the selected feeds and additives
		*/
		$scope.compare = function () {
			$scope.control.spinner = true;

			// get feed params
			var params1 = $scope.params.feed1;
			var params2 = $scope.params.feed2;

			// get feed object of selected feed
			var feed1 = $scope.data.country[vars.country].type[$scope.input.type].breed[$scope.input.breed].inputWeight[$scope.input.inputWeight].feed[$scope.input.feed1];
			var feed2 = $scope.data.country[vars.country].type[$scope.input.type].breed[$scope.input.breed].inputWeight[$scope.input.inputWeight].feed[$scope.input.feed2];

			// get additive object of selected additive
			var additive1 = $scope.input.additive1 ? $scope.data.country[vars.country].additives.type[$scope.input.additive1] : null;
			var additive2 = $scope.input.additive2 ? $scope.data.country[vars.country].additives.type[$scope.input.additive2] : null;

			// compute results values for feed 1
			var additive1Duration = additive1 ? feed1.fatteningDays * toFloat(additive1.duration) / 100 : 0;
			var additive1Gain = additive1Duration ? (additive1Duration * additive1.ADG / 1000) : 0;

			var outputWeight1 = toFloat($scope.input.inputWeight) + (feed1.fatteningDays * feed1.ADG / 1000) + additive1Gain;
			var sellingWeight1 = outputWeight1 * feed1.efficiency_carcass / 100;
			var sellingPrice1 = $scope.input.priceType1 ? toFloat($scope.input.sellingPrice) * sellingWeight1 : toFloat($scope.input.sellingPrice) * outputWeight1;

			$scope.comp.feed1Price = ((((toFloat(params1.cornSilage[0]) * toFloat(params1.cornSilage[1])) + (toFloat(params1.cereal[0]) * toFloat(params1.cereal[1])) +
			                         (toFloat(params1.aliment36[0]) * toFloat(params1.aliment36[1])) + (toFloat(params1.aliment25[0]) * toFloat(params1.aliment25[1])) + 
			                         (toFloat(params1.alimentComplete[0]) * toFloat(params1.alimentComplete[1]))) * feed1.fatteningDays) +
			                         (toFloat(params1.additive[0]) * additive1Duration)).toFixed(0);

			$scope.comp.feed1Profit = (sellingPrice1 - toFloat($scope.input.buyingPrice) - $scope.comp.feed1Price).toFixed(0);
			$scope.comp.fedPerPlace1 = ((365 - toFloat(params1.sanitaryPeriod)) / feed1.fatteningDays).toFixed(1);
			$scope.comp.placeProfit1 = ($scope.comp.fedPerPlace1 * $scope.comp.feed1Profit).toFixed(0);
			$scope.comp.fatteningDay1 = feed1.fatteningDays;

			// compute results values for feed 2
			var additive2Duration = additive2 ? feed2.fatteningDays * toFloat(additive2.duration) / 100 : 0;
			var additive2Gain = additive2Duration ? (additive2Duration * additive2.ADG / 1000) : 0;

			var outputWeight2 = toFloat($scope.input.inputWeight) + (feed2.fatteningDays * feed2.ADG / 1000) + additive2Gain;
			var sellingWeight2 = outputWeight2 * feed2.efficiency_carcass / 100;
			var sellingPrice2 = $scope.input.priceType1 ? toFloat($scope.input.sellingPrice) * sellingWeight2 : toFloat($scope.input.sellingPrice) * outputWeight2;

			$scope.comp.feed2Price = ((((toFloat(params2.cornSilage[0]) * toFloat(params2.cornSilage[1])) + (toFloat(params2.cereal[0]) * toFloat(params2.cereal[1])) +
			                         (toFloat(params2.aliment36[0]) * toFloat(params2.aliment36[1])) + (toFloat(params2.aliment25[0]) * toFloat(params2.aliment25[1])) + 
			                         (toFloat(params2.alimentComplete[0]) * toFloat(params2.alimentComplete[1]))) * feed2.fatteningDays) +
			                         (toFloat(params2.additive[0]) * toFloat(additive2Duration))).toFixed(0);

			$scope.comp.feed2Profit = (sellingPrice2 - toFloat($scope.input.buyingPrice) - $scope.comp.feed2Price).toFixed(0);
			$scope.comp.fedPerPlace2 = ((365 - toFloat(params2.sanitaryPeriod)) / feed2.fatteningDays).toFixed(1);
			$scope.comp.placeProfit2 = ($scope.comp.fedPerPlace2 * $scope.comp.feed2Profit).toFixed(0);
			$scope.comp.fatteningDay2 = feed2.fatteningDays;

			// compute profit values
			$scope.comp.feedProfit = ($scope.comp.feed2Profit - $scope.comp.feed1Profit).toFixed(0);
			$scope.comp.fatteningDayReduction = $scope.comp.fatteningDay2 - $scope.comp.fatteningDay1;

			$scope.control.spinner = false;
		}

		$scope.recalculate = function () {
		  	$scope.compare();

		  	//scroll to the top of the page to see new results of comparison
		    $ionicScrollDelegate.scrollTop(true);
		}

		$scope.toggleParams = function () {
			$scope.control.paramPanel = !$scope.control.paramPanel;
			// scroll up few lines to let user see beginning of params table
			$ionicScrollDelegate.scrollBy(0, 100, true);
		}

		$scope.compEnabled = function () {
			return $scope.input.type && $scope.input.breed && $scope.input.inputWeight && $scope.input.buyingPrice && $scope.input.sellingPrice && $scope.input.feed1 && $scope.input.feed2;
		}

		$scope.checkbox1Change = function () {
			$scope.input.priceType2 = ! $scope.input.priceType1;
		}
		$scope.checkbox2Change = function () {
			$scope.input.priceType1 = ! $scope.input.priceType2;
		}

		$scope.closeKeyboard = function (event) {
			if (event.charCode == 13) {
			    if (window.cordova && window.cordova.plugins.Keyboard) {
					cordova.plugins.Keyboard.close();
				}
			}
		}

		$scope.goBack = function() {
		    $ionicHistory.goBack();
		};

		$scope.validSettings = function() {
			$scope.setLanguage();

			vars.settings.language = $scope.input.language;
			animalsSvc.saveSettings();
		    $scope.goBack();
		};
	})

})();