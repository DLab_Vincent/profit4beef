import json

fd = open("fr_last_data.json", "r")
content = fd.read()
jsonContent = json.loads(content)

output = {}

for line in jsonContent:

	pays = 'France'

	# S'il n'existe pas, on cree "country": {}
	if not 'country' in output.keys():
		output['country'] = {}

	# S'il n'existe pas, on cree "Bresil": {} dans country
	if not pays in output['country'].keys():
		output['country'][pays] = {}

	# On recupere le type d'animal (vache, Macho Inteiro, etc)
	_type = line['type']

	# S'il n'existe pas, on cree "type": {} dans Bresil
	if not 'type' in output['country'][pays].keys():
		output['country'][pays]['type'] = {}

	# S'il n'existe pas, on cree le type d'animal dans type e.g. "vache": {} ou "Macho Inteiro": {}
	if not _type in output['country'][pays]['type'].keys():
		output['country'][pays]['type'][_type] = {}

	# On recupere la race de l'animal (Blonde, Charolaise, Nelore, gir-guzera-outras zebuinas, etc)
	race = line['race']

	# S'il n'existe pas, on cree "breed": {} dans le type d'animal precedemment cree
	if not 'breed' in output['country'][pays]['type'][_type].keys():
		output['country'][pays]['type'][_type]['breed'] = {}

	#  Si elle n'existe pas, on cree la race dans breed
	if not race in output['country'][pays]['type'][_type]['breed'].keys():
		output['country'][pays]['type'][_type]['breed'][race] = {}

  # On recupere le poids d'entree
	poids = line['poids entree']

	# S'il n'existe pas, on cree "inputWeight": {} dans la race
	if not 'inputWeight' in output['country'][pays]['type'][_type]['breed'][race].keys():
		output['country'][pays]['type'][_type]['breed'][race]['inputWeight'] = {}

	# S'il n'existe pas on cree le poids d'entree
	if not poids in output['country'][pays]['type'][_type]['breed'][race]['inputWeight'].keys():
		output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids] = {}

	# On recupere la ration
	ration = line['ration']

	# S'il n'existe pas on cree  "feed": {}
	if not 'feed' in output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids].keys():
		output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'] = {}

	# On cree la ration dans feed e.g. "aliment complet": {}
	if not ration in output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'].keys():
		output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration] = {}

	# On recupere le nombre de jours d'engraissement
	nb_jours_engraissement = line['nb_jours_engraissement']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['fatteningDays'] = nb_jours_engraissement

	# On recupere le GMQ
	GMQ = line['GMQ']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['ADG'] = GMQ

	# On recupere le rdt
	rdt_carcasse = line['rdt']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['efficiency_carcass'] = rdt_carcasse

  # On recupere le le coef prix carcasse
	coef_prix_carcasse = line['coef_prix_carcasse']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['coef_prix_carcasse'] = coef_prix_carcasse

	# We add rations

	ensilage = line['mais']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['ensilage'] = ensilage

	cereales = line['cereales']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['cereals'] = cereales

	aliment_36 = line['aliment36']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['aliment_36'] = aliment_36

	aliment_25 = line['aliment25']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['aliment_25'] = aliment_25

	aliment_complet = line['complet']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['aliment_complete'] = aliment_complet

	fourrage = line['fourrage']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['forrage'] = fourrage

  # Here we fix all price for rations
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['ensilage_price'] = 25
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['cereals_price'] = 180
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['aliment_36_price'] = 400
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['aliment_25_price'] = 330
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['aliment_complete_price'] = 280
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['forrage_price'] = 60


print json.dumps(output, sort_keys=True, indent=4, separators=(',', ': '))

