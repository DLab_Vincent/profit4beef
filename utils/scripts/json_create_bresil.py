import json

fd = open("br_last_data.json", "r")
content = fd.read()
jsonContent = json.loads(content)

output = {}

for line in jsonContent:

	pays = 'Bresil'

	# S'il n'existe pas, on cree "country": {}
	if not 'country' in output.keys():
		output['country'] = {}

	# S'il n'existe pas, on cree "Bresil": {} dans country
	if not pays in output['country'].keys():
		output['country'][pays] = {}

	# On recupere le type d'animal (vache, Macho Inteiro, etc)
	_type = line['type']

	# S'il n'existe pas, on cree "type": {} dans Bresil
	if not 'type' in output['country'][pays].keys():
		output['country'][pays]['type'] = {}

	# S'il n'existe pas, on cree le type d'animal dans type e.g. "vache": {} ou "Macho Inteiro": {}
	if not _type in output['country'][pays]['type'].keys():
		output['country'][pays]['type'][_type] = {}

	# On recupere la race de l'animal (Blonde, Charolaise, Nelore, gir-guzera-outras zebuinas, etc)
	race = line['race']

	# S'il n'existe pas, on cree "breed": {} dans le type d'animal precedemment cree
	if not 'breed' in output['country'][pays]['type'][_type].keys():
		output['country'][pays]['type'][_type]['breed'] = {}

	#  Si elle n'existe pas, on cree la race dans breed
	if not race in output['country'][pays]['type'][_type]['breed'].keys():
		output['country'][pays]['type'][_type]['breed'][race] = {}

	# On recupere le poids d'entree
	poids = line['poids entree']

	# S'il n'existe pas, on cree "inputWeight": {} dans la race
	if not 'inputWeight' in output['country'][pays]['type'][_type]['breed'][race].keys():
		output['country'][pays]['type'][_type]['breed'][race]['inputWeight'] = {}

	# S'il n'existe pas on cree le poids d'entree
	if not poids in output['country'][pays]['type'][_type]['breed'][race]['inputWeight'].keys():
		output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids] = {}

	# On recupere la ration
	ration = line['ration']

	# S'il n'existe pas on cree  "feed": {}
	if not 'feed' in output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids].keys():
		output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'] = {}

	# On cree la ration dans feed e.g. "aliment complet": {}
	if not ration in output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'].keys():
		output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration] = {}

	# On recupere le nombre de jours d'engraissement
	nb_jours_engraissement = line['nb_jours_engraissement']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['fatteningDays'] = nb_jours_engraissement

	# On recupere le GMQ
	GMQ = line['GMQ']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['ADG'] = GMQ

	rdt_carcasse = line['rdt']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['efficiency_carcass'] = rdt_carcasse

	# coef_prix_carcasse is intended to be used later but for now (20/10/2017), it's fixed to 1
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['coef_prix_carcasse'] = 1

	# On ajoute les rations alimentaires
	nucleo_grao_inteiro = line['nucleo_grao_inteiro']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['nucleo_grao_inteiro'] = nucleo_grao_inteiro

	milho_grao_inteiro = line['milho_grao_inteiro']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['milho_grao_inteiro'] = milho_grao_inteiro

	racao_concentra = line['racao_concentra']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['racao_concentra'] = racao_concentra

	alimento_volumoso = line['alimento_volumoso']
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['alimento_volumoso'] = alimento_volumoso

	# Here we fix all price for rations
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['nucleo_grao_inteiro_price'] = 1700
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['milho_grao_inteiro_price'] = 480
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['racao_concentra_price'] = 920
	output['country'][pays]['type'][_type]['breed'][race]['inputWeight'][poids]['feed'][ration]['alimento_volumoso_price'] = 100


print json.dumps(output, sort_keys=True, indent=4, separators=(',', ': '))


